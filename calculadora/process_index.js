const{createApp} = Vue;
createApp
({
    data()
    {
        return{
            display:"0",
            operandoAtual:null,
            operandoAnterior:null,
            operador:null,
        };//Fechamento do return
    },//Fechamento da função "data"

    methods:{
        handleButtonClick(botao){
            switch(botao){
                case "+":
                case "-":
                case "/":
                case "*":

                    this.handleOperation(botao);
                    break;

                case "=":
                    this.handleEquals();
                    break;
                    
                case ".":
                    this.handleDecimal();
                    break;

                default:
                    this.handleNumber(botao);
                    break;
            }//Fechamento switch
        },//Fim do handleButtonClick
        handleNumber(numero){
            if(this.display==="0")
            {
                this.display = numero.toString();
            }
            else
            {
                this.display += numero.toString();
            }
        },//Fechamento handleNumber

        handleOperation(operacao){
            if(this.operandoAtual !== null)
            {
                this.handleEquals();
            }
            this.operador = operacao;

            this.operandoAtual = parseFloat(this.display);
            this.display = "0";

        },//Fechamento handleOperation

        handleEquals()
        {
            const displayAtual = parseFloat(this.display);
            if(this.operandoAtual !== null && this.operador !== null)
            {
                switch(this.operador)
                {
                    case "+":
                        this.display = (this.operandoAtual + displayAtual).toString();
                            break;
                    case "-":
                        this.display = (this.operandoAtual - displayAtual).toString();
                            break;
                    case "/":
                        this.display = (this.operandoAtual / displayAtual).toString();
                            break;
                        case "*":
                        this.display = (this.operandoAtual * displayAtual).toString();
                            break;
                }//Fim do switch

                this,operandoAnterior = this.operandoAtual;
                this.operandoAtual = null;
                this.operador = null;
            }//Fechamento do if
            else
            {
                this.operandoAnterior = displayAtual;
            }//Fechamento do else

        },//Fechamento handleEquals

        handleDecimal()
        {
            if(!this.display.includes("."))
            {
                this.display += ".";

            }//fechando if
        },//fechando handleDecimal 

        handleClear()
        {
            this.display = "0";
            this.operandoAnterior = null;
            this.operandoAtual = null;
            this.operador = null; 

        },//fechando handleClear 
    },//Fim methods
}).mount("#app"); //Fechamento do "createApp"
