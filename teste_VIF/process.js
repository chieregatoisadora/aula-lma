const {createApp}= Vue;
createApp ({
    data(){
        return{
            testeSpan:false,
            isLampadaLigada:false,
        }//fim return
    },//fim data 
    
    methods:{
        handleTest:function()
        {
            this.testeSpan = !this.testeSpan;
        },//fim handleTest

        toogleLampada:function()
        {
            this.isLampadaLigada=!this.isLampadaLigada
        }//fim toogleLampada
    },//fim methods
}).mount("#app")