const {createApp} = Vue;
createApp({
    data(){
        return{
            username:'',
            password:'',
            error: null,
            correct: null,

            //arrays para armazenamento dos usuários e senhas
            usuarios: ["admin", " isadora"],
            senhas: ["123", "1234"],

            //variáveis para armazenamento de usuário e senha que será cadastrado
            newUsername: "",
            newPassword: "",
            confirmPassword: "",
        };// fim return
    },// fim data

    methods: {
        login(){
            setTimeout(() => {
                if((this.username === 'maria' && this.password === '1231')){
                        alert ('Login realizado ')
                } //fim do if
                else{
                    this.error = "Nome ou senha incorretos!"
                };
            }, 1000);
        }, // fechamento Login

        adicionarUsuario(){

            if(!this.usuarios.includes(this.newUsername) && this.newUsername !== ""){

                if(this.newPassword && this.newPassword === this.confirmPassword){
                    this.usuarios.push(this.newUsername);
                    this.senhas.push(this.newPassword);

                    //atualizando os valores dos arrays no armazenamento local
                    localStorage.setItem("usuarios" , JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas" , JSON.stringify(this.senhas));
                    this.newUsername="";
                    this.newPassword="";
                    this.confirmPassword="";

                    this.correct = "Cadastro realizado com sucesso";
                    this.error = null;
                }//Fechamento if

                else{
                    this.error = "Por favor, digite uma senha válida!";
                    this.correct = null;
                }
            }//fechamento if
            else{
                this.error = "Por favor, informe um usuário válido!";
                this.correct = null;
            }//Fechamento else
        },//Fechamento adicionarUsuario
    }, // fechamento methods
}).mount("#app");// fechamento createApp