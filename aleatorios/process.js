const{createApp}= Vue;
createApp
({
    data()
    {
        return{
            randomIndex:0,
            randomIndexInternet:0,

            //vetor de imagens locais

            imagensLocais:[
                './img/lua.jpg',
                './img/sol.jpg',
            ],
            ImageInternet:[
                'https://img.freepik.com/vetores-gratis/bela-casa_24877-50819.jpg',
                'https://caras.uol.com.br/media/uploads/legacy/2018/09/22/neymar-jr-822403.jpg',
                'https://img.freepik.com/vetores-gratis/predio-de-apartamentos-e-arvores-isometricas_603843-337.jpg?w=2000',
            ],

        };//fim do return
    },//fim data

    computed:{
        randomImage()
        {
            return this.imagensLocais [this.randomIndex]
        },//fim randomImage

        randomImageInternet()
        {
            return this.ImageInternet[this.randomIndexInternet]//retorna o conteudo da posição que esta armazenado no indexinternet
        },//fim randomImageInternet
    },//fim computed

    methods:{
        getRandomImage()
        {
            this.randomIndex = Math.floor(Math.random()*this.imagensLocais.length);//não coloca o mais um porque ta usando a função length
            this.randomIndexInternet = Math.floor(Math.random()*this.ImageInternet.length); 
        }//fim getImage
    },//fim methods
    
}).mount("#app");